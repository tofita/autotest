package com.autotest.zprice.tests;

import com.github.javafaker.Faker;
import com.autotest.zprice.test.base.BaseTest;
import com.autotest.zprice.test.helpers.NavigationBar;
import com.autotest.zprice.test.pages.Goods;
import com.autotest.zprice.test.pages.Index;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class AddProduct extends BaseTest {

    final private static Faker faker = new Faker();
    final private Goods productsPage = new Goods();
    final private String clientCode = faker.number().digits(10),
            modelGroup = faker.company().industry(),
            modelBrand = faker.company().buzzword(),
            modelName = faker.pokemon().name(),
            productUrl = faker.internet().url();
    final private int productPrice = faker.number().numberBetween(1000, 222222);
    final private String negativeProductPrice = faker.artist().name();
    final private String productSummary = modelGroup + " " + modelBrand + " " + modelName;

    @Feature("Ручне додавання товару.")
    @Test
    public void addManualProduct() {
        Index.signIn("testAccountForAutoTest", "1234567");
        new NavigationBar().openProductsPage();

        productsPage
                .clickOnAddNewProductButton()
                .clickOnAddProductButton()
                .enterClientCode(clientCode)
                .enterProductBrand(modelBrand)
                .enterProductName(modelName)
                .enterPrice(productPrice)
                .enterProductGroup(modelGroup)
                .saveProduct();

        productsPage.isProductCreated(productSummary);

        deleteProduct();
        deleteAllProductsFromArchive();
    }

    @Feature("Додавання прайс-листа")
    @Test
    public void uploadPrices() {
        Index.signIn("user_for_tests", "000000");
        new NavigationBar().openProductsPage();

        String PRICE_DEMO_LIMIT = "src/main/resources/prices/priceExample5sku.xlsx";

        productsPage
                .clickOnAddNewProductButton()
                .clickOnPriceAutoUploadingButton()
                .selectPriceFormatExcel()
                .selectImportTypeFromFile()
                .uploadPriceListFile(PRICE_DEMO_LIMIT)
                .clickNext()
                .deleteFirstPosition()
                .selectCorrectColumnsForExcelPrice()
                .clickNext()
                .clickNext()

                .isFileUploaded()

                .clickOnDoneButton()
                .deleteAllProductsFromTable();
    }

    @Step("Удалить созданный продукт.")
    protected void deleteProduct() {
        $$x("//span[contains(@class, 'dx-checkbox-icon')]").first().click();
        $x("//i[contains(@class, 'dx-icon far fa-trash-alt')]").click();

        $(".dx-datagrid-nodata").waitUntil((text("No data")), 5000);
    }

    @Step("Удалить продукты из архива.")
    protected void deleteAllProductsFromArchive() {
        sleep(3000);
        $(".fa-archive").click();
//        sleep(3000);
        $("[data-bind] .dx-checkbox-icon").waitUntil(visible, 7000).click();
        $("[data-bind] .ng-star-inserted [role='button']:nth-of-type(2)")
                .waitUntil(visible, 20000).click();
        $("[data-bind] .dx-datagrid-nodata").waitUntil(text("Записи в архиве отсутствуют"), 60000);
    }
}