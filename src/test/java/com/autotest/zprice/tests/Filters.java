package com.autotest.zprice.tests;

import com.autotest.zprice.test.base.BaseTest;
import com.autotest.zprice.test.helpers.NavigationBar;
import com.autotest.zprice.test.pages.Index;
import com.autotest.zprice.test.pages.Stores;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;

public class Filters extends BaseTest {

    private final Stores filterBar = new Stores();

    @Feature("Фильтр по меткам товаров.")
    @Test
    public void canFilterByProductTag() {
        Index.signIn("zlogin", "111111");
        new NavigationBar().openProductsPage();

        filterBar
                .filterByProductTag()
                .isFilteredByProductTag();

    }

    @Feature("Фильтр Группа + Бренд.")
    @Test
    public void canFilterByGroupAndBrand() {
        Index.signIn("zlogin", "111111");
        new NavigationBar().openProductsPage();

        filterBar

                .selectBrand()
                .selectGroup()
                .selectRRC()

                .isFilteredByGroupsAndBrands();

    }
}