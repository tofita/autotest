package com.autotest.zprice.tests;

import com.github.javafaker.Faker;
import com.autotest.zprice.test.base.BaseTest;
import com.autotest.zprice.test.helpers.NavigationBar;
import com.autotest.zprice.test.pages.Index;
import com.autotest.zprice.test.pages.Stores;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;

public class StoresFilteringActions extends BaseTest {

    final private Stores storesPage = new Stores();
    final private Stores filterBar = new Stores();
    final private Faker faker = new Faker();

    @Feature("Весь рынок. Фильтр по магазинам.")
    @Test
    public void canFilterByStoresWholeMarket() {
        Index.signIn("demouser", "000000qq");
        new NavigationBar().openStoresPage();

        filterBar
                .filterByStoreFromWholeMarketList()
                .isFilteredBySiteWM();
    }

    @Feature("Мои магазины. Фильтр по магазинам.")
    @Test
    public void canFilterByMyStores() {
        Index.signIn("zlogin", "111111");
        new NavigationBar().openStoresPage();

        filterBar
                .filterByStoreFromMyStoresList()
                .isFilteredBySite();
    }

    @Feature("Фильтр по меткам магазинов.")
    @Test
    public void canFilterByStoresTags() {
        Index.signIn("zlogin", "111111");

        filterBar
                .filterByStoresTags()
                .isFilteredByStoreTag()

                .resetAllFilters();
    }

    @Feature("Фильтр по меткам товаров на вкладке 'Представленные товары'.")
    @Test
    public void canFilterByProductsTags() {
        Index.signIn("zlogin", "111111");
        new NavigationBar().openStoresPage();

        filterBar
                .filterByProductTag()
                .expandStoresInfo(0)
                .isProductsFilteredByTag();
    }

    @Feature("Поиск магазина в таблице.")
    @Test
    public void canFindStoreInTableList() {
        Index.signIn("zlogin", "111111");
        new NavigationBar().openStoresPage();

        String site = "worktool.com.ua";
        storesPage
                .enterSiteName(site)
                .isSiteDisplayedInTable(site);
    }
}