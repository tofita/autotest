package com.autotest.api;

import com.autotest.api.actions.API;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;

public class TestingAPI {

    API API = new API();
    int success = 200;


    @Feature("Додати товар")
    @Test
    public void addProducts(){
        API.addProducts(success);
    }

    @Feature("Отримати товар")
    @Test
    public void getProducts(){
        API.getProducts(success);
    }

    @Feature("Додати магазин")
    @Test
    public void addStores(){
        API.addStores(success);
    }

    @Feature("Отримати усі магазини")
    @Test
    public void getStores(){
        API.getStores(success);
    }

    @Feature("Видалити магазин")
    @Test
    public void deleteStores(){
        API.deleteStores(success);
    }
}
