package com.autotest.api.actions;

import com.github.javafaker.Faker;
import org.json.JSONArray;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;

public class API {
    public String clientCode = "1375724";
    public String name = "iPhone 12 128GB Black Офіційна гарантія";
    public String group = "Мобільний телефон";
    public String brand = "Apple";
    public int rrp = 31779;


    public void addProducts(int statusCode) {

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        jsonObject.put("ClientCode", clientCode);
        jsonObject.put("Name", name);
        jsonObject.put("Group", group);
        jsonObject.put("Brand", brand);
        jsonObject.put("RRP", rrp);

        jsonArray.put(jsonObject);

        given()
                .header("apiKey", "1d1194c1-98f4-4fe6-a592-e56c6c39ebca")
                .header("Content-Type", "application/json")
                .body(jsonArray
                        .toString())
                .post("http://api.z-price.com/Products")
                .then().log().body()
                .and().assertThat()
                .statusCode(statusCode);
    }

    public void getProducts(int statusCode) {

        JSONObject mainData = new JSONObject();
        JSONObject options = new JSONObject();
        JSONArray skuArray = new JSONArray();
        JSONObject skuValue = new JSONObject();

        skuValue.put("Name", clientCode);

        skuArray.put(skuValue);

        options.put("Skus", skuArray);

        mainData.put("Sample", "All");
        mainData.put("Type", "JSON");
        mainData.put("ForAllMyCompanies", false);
        mainData.put("Options", options);

        System.out.println(mainData);
        given()
                .header("apiKey", "1d1194c1-98f4-4fe6-a592-e56c6c39ebca")
                .header("Content-Type", "application/json")
                .body(mainData
                        .toString())
                .post("http://api.z-price.com/Report")
                .then().log().body()
                .and().assertThat()
                .statusCode(statusCode);
    }

    public void addStores(int statusCode){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Name", "allo.ua");

        given()
                .header("apiKey", "1d1194c1-98f4-4fe6-a592-e56c6c39ebca")
                .header("Content-Type", "application/json")
                .body(jsonObject
                        .toString())
                .post("http://api.z-price.com/Stores")
                .then().log().body()
                .and().assertThat()
                .statusCode(statusCode);
    }

    public void getStores(int statusCode){
        given()
                .header("apiKey", "1d1194c1-98f4-4fe6-a592-e56c6c39ebca")
                .header("Content-Type", "application/json")
                .get("http://api.z-price.com/Stores")
                .then().log().body()
                .and().assertThat()
                .statusCode(statusCode);
    }

    public void deleteStores(int statusCode){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Name", "allo.ua");

        given()
                .header("apiKey", "1d1194c1-98f4-4fe6-a592-e56c6c39ebca")
                .header("Content-Type", "application/json")
                .body(jsonObject
                        .toString())
                .delete("http://api.z-price.com/Stores")
                .then().log().body()
                .and().assertThat()
                .statusCode(statusCode);
    }
}
