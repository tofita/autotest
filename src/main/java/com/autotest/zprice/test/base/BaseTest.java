package com.autotest.zprice.test.base;

import com.codeborne.selenide.logevents.SelenideLogger;
import com.autotest.zprice.test.helpers.TestListener;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.*;

@Listeners({TestListener.class})
public class BaseTest {

    @BeforeMethod
    @Step("Настройка параметров запуска автотестов.")
    public void setUp() {
        System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");

        baseUrl = "https://z-price.com";
        startMaximized = false;
        browserSize = "1920x1080";
        timeout = 60000;

        System.setProperty("webdriver.chrome.driver", "src/driver/chromedriver.exe");

        savePageSource = false;
        SelenideLogger
                .addListener("AllureSelenide", new AllureSelenide()
                        .screenshots(true).savePageSource(false));
        fastSetValue = true;

        open(baseUrl);
    }

    @AfterMethod
    @Step("Закрыть браузер.")
    public void tearDown() {
        closeWebDriver();
    }
}