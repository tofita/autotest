package com.autotest.zprice.test.helpers;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.executeJavaScript;

public class Action {

    public static void click(SelenideElement element) {
        executeJavaScript("arguments[0].click();", element.waitUntil(visible, 15000));
    }

    public static void select(SelenideElement dropdown, SelenideElement option) {
        dropdown.waitUntil(visible, 10000).click();
        option.waitUntil(visible, 10000).click();
    }

    public static void sendText(SelenideElement element) {
        executeJavaScript("arguments[0].value = 'x'.repeat(arguments[1])", element, 200000);
    }

    public static void openNewTab() {
        executeJavaScript("window.open(arguments[0])");
    }

    public static void openNewTab(String url) {
//        executeJavaScript("window.open(arguments[0], arguments[1]);", url, "_blank");
        executeJavaScript("window.open(arguments[0], arguments[1]);", url);
    }

    public static void setDateByName(String name, String date) {
        executeJavaScript(
                String.format("$('[name=\"%s\"]').val('%s')", name, date)
        );
    }
}