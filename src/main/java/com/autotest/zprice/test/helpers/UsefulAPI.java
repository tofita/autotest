package com.autotest.zprice.test.helpers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.restassured.RestAssured.given;

public class UsefulAPI {

    public String generateStringFromResource(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public void sendJSON () throws IOException {

        String json = "src/main/resources/trashFile/";
        String jsonBody =  generateStringFromResource(json);

        given()
                .header("Accept", "application/json, text/plain, */*")
                .header("Content-Type", "application/json")
                .header("Accept-Encoding", "gzip, deflate")
                .header("apiKey", "f1b88156-5386-41d2-8dcc-b73318cbd329")
                .body(jsonBody)
                .post("http://api.z-price.com/Products")
                .then().log().body()
                .and().assertThat().statusCode(200);
    }
}
