package com.autotest.zprice.test.helpers;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestRetryAnalyzer implements IRetryAnalyzer {

    private int counter = 1;

    public boolean retry(ITestResult result) {
        int retryMaxLimit = 2;
        if (counter < retryMaxLimit) {
            counter++;
            return true;
        }
        return false;
    }
}