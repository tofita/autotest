package com.autotest.zprice.test.helpers;

import com.autotest.zprice.test.pages.Goods;
import com.autotest.zprice.test.pages.Stores;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class NavigationBar {

    final private SelenideElement
            productsLink = $x("//a[@id='header_productsPageBtn']"),
            storesLink = $x("//a[@id='header_indexPageBtn']"),
            accountDetailsDropdown = $("#accountDetailsBtn"),
            headerLogo = $(".top_header__logo"),
            billingPageIcon = $x("//a[@id='header_billingPageBtn']");


    @Step("Відкрити сторінку 'Товари'.")
    public Goods openProductsPage() {
        productsLink.click();
        return page(Goods.class);
    }

    @Step("Открыть страницу 'Магазины'.")
    public Stores openStoresPage() {
        storesLink.click();
        return page(Stores.class);
    }

    @Step("Выйти из аккаунта.")
    public void logoutDev() {
        sleep(1000);
        accountDetailsDropdown.click();
        sleep(1000);
        $("[data-bind] .dx-template-wrapper .mediumText:nth-child(6)").click();
        headerLogo.waitUntil(visible, 8000);
    }

    @Step("Выйти из аккаунта.")
    public void logoutUser() {
        sleep(1000);
        accountDetailsDropdown.click();
        sleep(1000);
        $("[data-bind] .dx-template-wrapper .mediumText:nth-child(5)").click();
        headerLogo.waitUntil(visible, 8000);
    }

}