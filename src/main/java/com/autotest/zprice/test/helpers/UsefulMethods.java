package com.autotest.zprice.test.helpers;

import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selenide.$x;

public class UsefulMethods {

    public static boolean getLoaderStatus(){
        return $x("//app-loader[@class='whiteTxt']").waitUntil(Condition.attribute("hidden"), 60000).getAttribute("hidden") != null;
    }

}
