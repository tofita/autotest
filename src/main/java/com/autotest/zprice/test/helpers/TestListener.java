package com.autotest.zprice.test.helpers;

import io.qameta.allure.Attachment;
import org.openqa.selenium.logging.LogEntry;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.logging.Level;
import java.util.stream.Collectors;

import static com.codeborne.selenide.WebDriverRunner.*;

public class TestListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        getConsoleLog();

//        if (isChrome()) {
//            getConsoleLog();
//        } else {
//            System.out.println("logger is disabled");
//        }
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        if (isChrome()) {
            getConsoleLog();
            System.out.println("--------------------------Something went wrong. Test was skipped!");
        } else {
            System.out.println("Is not a Chrome");
        }
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }


    @Attachment(value = "consoleLog", type = "text/json")
    private static String getConsoleLog() {
        return getWebDriver().manage().logs().get("browser").getAll().stream().
                filter(LogEntry -> LogEntry.getLevel().equals(Level.SEVERE)).map(LogEntry::toString).
                collect(Collectors.joining("\n"));
    }
}