package com.autotest.zprice.test.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.testng.Assert;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class Goods {

    final private SelenideElement
            addNewProductBtn = $("#headerFixedToolbar > [role='button']:nth-child(3)"),

            addProductManuallyBtn = $("div.dx-popup-content > div > div > div:nth-child(1) > dx-button"),
            clientCode =$x("//div/div[2]/dx-data-grid//div[6]//table/tbody/tr[1]/td[2]/div[1]//input"),
            modelGroup = $x("//div/div[2]/dx-data-grid//div[6]//table/tbody/tr[1]/td[3]/div[1]//input"),
            modelBrand = $x("//div/div[2]/dx-data-grid//div[6]//table/tbody/tr[1]/td[4]/div[1]//input"),
            modelName = $x("//div/div[2]/dx-data-grid//div[6]//table/tbody/tr[1]/td[5]/div[1]//input"),
            productPrice = $x("//div/div[2]/dx-data-grid//div[6]//table/tbody/tr[1]/td[7]/div[1]//input"),
            productLink = $x("//div/div[2]/dx-data-grid//div[6]//table/tbody/tr[1]/td[6]/div[1]//input"),

            saveProductBtn = $("[data-bind] [role='toolbar']:nth-of-type(3) " +
                    ".dx-toolbar-button:nth-of-type(1) [role]"),

            autoUploadingButton = $(".dx-template-wrapper .ng-star-inserted:nth-of-type(1) " +
                    ".ng-star-inserted:nth-of-type(1) div:nth-of-type(1) [role]"),

            priceFormatIconXml = $("[title='XML']"),
            priceFormatIconExcel = $("[title='Excel']"),

            importTypeByUrl = $("[for='importTypeByUrl']"),
            importTypeManual = $("[for='importTypeManual']"),

            priceLinkInput = $("global-products-price [type='text']"),
            uploadPriceButton = $(".btn-primary"),

            fileInput = $(".dx-fileuploader-input"),
            fileNameString = $(".dx-fileuploader-file-name"),

            nextBtn = $("[data-bind] .dx-toolbar-button:nth-of-type(2) [role]"),
            doneBtn = $("[data-bind] .dx-toolbar-button:nth-of-type(3) [role]"),

            //columns
            clientCodeColumn = $("#dx-col-23 > div > div > div > dx-select-box > div > div"),
            productTitleColumn = $("#dx-col-24 > div > div > div > dx-select-box > div > input[type=hidden]"),
            recommendedPriceColumn = $("td#dx-col-25 div > div > input"),
            productGroupColumn = $("td#dx-col-26 div > div > input"),
            productBrandColumn = $("td#dx-col-27 div > div > input"),

            resultsPreviewTable = $("global-products-price [role] [role='presentation']:nth-of-type(6)"),
            loader = $("global-products-price app-loader .ng-star-inserted div");

    private ElementsCollection itemsOnPage() {
        return $$(".dx-page-sizes [role='button']");
    }

 //For auto uploading
    @Step("Удалить добавленные товары из списка.")
    public void deleteAllProductsFromTable() {
//        itemsOnPage().last().waitUntil(visible, 60000).click();
        sleep(3000); //3s
        $("app-brands-page-products h4:nth-child(3)").scrollTo();
        $("[role] [role='presentation']:nth-of-type(5) .dx-checkbox-icon").click();
        $("#headerFixedToolbar .ng-star-inserted:nth-of-type(2) [role='button']:nth-of-type(2) " +
                ".dx-button-content").click();
        $(".dx-datagrid-nodata").waitUntil(enabled,20000);
    }

    @Step("Файл успешно загружен.")
    public Goods isFileUploaded() {
        $("global-products-price h4").waitUntil(text("Прайс-лист успешно загружен!"),60000);
        return this;
    }

    @Step("Выбрать формат прайс-листа[Excel]")
    public Goods selectPriceFormatExcel() {
        priceFormatIconExcel.click();
        return this;
    }

    @Step("Вибрати і завантажити файл прайс-листа.")
    public Goods uploadPriceListFile(String priceList) {
        fileInput.uploadFile(new File(priceList));
        return this;
    }

    @Step("Натиснути кнопку 'Автоматичне завантаження прайс-листа'.")
    public Goods clickOnPriceAutoUploadingButton() {
        sleep(1000); //2s
        autoUploadingButton.click();
        return this;
    }

    @Step("Выбрать формат прайс-листа[XML]")
    public Goods selectPriceFormatXml() {
        priceFormatIconXml.click();
        return this;
    }

    @Step("Выбрать вид импорта.")
    public Goods selectImportTypeByLink() {
        importTypeByUrl.click();
        return this;
    }

    @Step("Выбрать вид импорта.")
    public Goods selectImportTypeFromFile() {
        importTypeManual.click();
        return this;
    }

    @Step("Указать ссылку на прайс-лист.")
    public Goods enterLinkToPriceList(String priceLink) {
        priceLinkInput.setValue(priceLink);
        return this;
    }

    @Step("Нажать кнопку 'Загрузить'.")
    public Goods clickOnUploadPriceButton() {
        uploadPriceButton.click();
        return this;
    }

    @Step("Нажать кнопку 'Вперед'.")
    public Goods clickNext() {
        loader.waitWhile(visible,300000);
        nextBtn.click();
        return this;
    }

    @Step("Нажать кнопку 'Готово'.")
    public Goods clickOnDoneButton() {
//        $("[data]").waitUntil(be(not(visible)), 60000);
//        doneBtn.click();
        sleep(5000);
        doneBtn.waitUntil(visible, 120000).click();
        return this;
    }

    @Step("Выбрать колонки соответсвтующие прайс-листу.")
    public Goods selectCorrectColumnsForExcelPrice() {
        $("global-products-price [aria-selected='false']:nth-of-type(2) .dx-dropdowneditor-icon").click();
        $(".dx-scrollview-content [role='option']:nth-of-type(4) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(3) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(5) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(4) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(7) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(5) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(2) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(6) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(3) .dx-list-item-content").click();
        return this;

    }

    @Step("Выбрать колонки соответсвтующие прайс-листу.")
    public Goods selectCorrectColumns() {
        $("global-products-price [aria-selected='false']:nth-of-type(3) .dx-dropdowneditor-icon").click();
        $(".dx-scrollview-content [role='option']:nth-of-type(2) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(4) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(4) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(5) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(5) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(6) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(3) .dx-list-item-content").click();

        $("global-products-price [aria-selected='false']:nth-of-type(8) .dx-dropdowneditor-icon").click();
        $("body [data-bind='dxControlsDescendantBindings\\: true']:nth-child(7) [role='option']" +
                ":nth-of-type(7) .dx-list-item-content").click();
        return this;
    }

/** For manually uploading */
    @Step("Добавить количество товаров, превышающее указаный лимит.")
    public Goods addProductsMoreThanLimit(int limit, String code, String group, String brand, String name, int price) {
        clickOnAddNewProductButton();
        for (int i = 0; i < limit + 1; i++) {
            addProduct(code, group, brand, name, price);
        }
        saveProduct();
        sleep(2000);
        $("[data-bind] .dx-toolbar-button:nth-of-type(2) .dx-button-text").click();
        return this;
    }

    @Step("Добавить товар вручную")
    public void addProduct(String code, String group, String brand, String name, int price) {
        clickOnAddProductButton();
        enterClientCode(code);
        enterProductGroup(group);
        enterProductBrand(brand);
        enterProductName(name);
        enterPrice(price);
        saveItem();
    }

    @Step("Нажать кнопку 'Добавить товары'.")
    public Goods clickOnAddNewProductButton() {
        addNewProductBtn.click();
        return this;
    }

    @Step("Нажать кнопку 'Добавить товар'.")
    public Goods clickOnAddProductButton() {
        sleep(1000); //2s
        addProductManuallyBtn.click();
        sleep(2000);
        return this;
    }

    @Step("Ввести клиентский код.")
    public Goods enterClientCode(String code) {
        $x("//div[2]/dx-data-grid/div/div[6]/div/table/tbody/tr[1]/td[2]").click();
        clientCode.setValue(code);
        return this;
    }

    @Step("Нажать наполе клиентский код")
    public Goods clientCodeClickPlace(){
        clientCode.click();
        return this;
    }

    @Step("Нажать наполе наименование")
    public Goods modelNameClickPlace(){
        modelName.click();
        return this;
    }

    @Step("Нажать наполе наименование")
    public Goods modelBrandClickPlace(){
        modelBrand.click();
        return this;
    }

    @Step("Нажать наполе РРЦ")
    public Goods productPriceClickPlace(){
        productPrice.click();
        return this;
    }

    @Step("Указать группу товара.")
    public Goods enterProductGroup(String group) {
        $x("//div[2]/dx-data-grid/div/div[6]/div/table/tbody/tr[1]/td[3]").click();
        modelGroup.setValue(group);
        return this;
    }

    @Step("Указать бренд товара.")
    public Goods enterProductBrand(String brand) {
        $x("//div[2]/dx-data-grid/div/div[6]/div/table/tbody/tr[1]/td[4]").click();
        modelBrand.setValue(brand);
        return this;
    }

    @Step("Указать наименование товара.")
    public Goods enterProductName(String product) {
        $x("//div[2]/dx-data-grid/div/div[6]/div/table/tbody/tr[1]/td[5]").click();
        modelName.setValue(product);
        return this;
    }

    @Step("Указать РРЦ.")
    public Goods enterPrice(int price) {
        $("[data-bind] [role] .dx-visibility-change-handler:nth-of-type(7)").click();
        productPrice.setValue(String.valueOf(price));
        return this;
    }

    @Step("Указать РРЦ.")
    public Goods enterNegativePrice(String price) {
        $x("//td[@class='dx-datagrid-validator dx-validator dx-visibility-change-handler dx-editor-cell dx-focused']//input[@class='dx-texteditor-input']").click();
        productPrice.setValue(String.valueOf(price));
        return this;
    }

    @Step("Нажать на поле РРЦ")
    public Goods negativePriceClick (){
        productPrice.click();
        return this;
    }

    @Step("Проверить вышла ли ошибка, что поле РРЦ должно иметь только число")
    public Goods rrcShouldHaveOnlyNumber(){
        String text = $x("//div[@class='dx-overlay-content dx-resizable']").getText();
        Assert.assertEquals(text, "В поле РРЦ может быть только число");
        return this;
    }

    @Step("Проверить вышла ли ошибка, что Поле обязательное к заполнению")
    public Goods thisPlaceRequired(){
        String text = $x("//div[@class='dx-overlay-content dx-resizable']").getText();
        Assert.assertEquals(text, "Поле обязательное к заполнению");
        return this;
    }

    @Step("Указать ссылку на продукт.")
    public Goods enterLinkToProduct(String link) {
        $x("//div[2]/dx-data-grid/div/div[6]/div/table/tbody/tr[1]/td[7]").click();
        productLink.setValue(link);
        return this;
    }

    @Step("Сохранить строку характеристик товара")
    public Goods saveItem() {
        $("td.dx-command-edit.dx-command-edit-with-icons > " +
                "a.dx-link.save-item-price.dx-icon.fas.fa-save").click();
        return this;
    }

    @Step("Нажать кнопку 'Сохранить'.")
    public void saveProduct() {
        $("td:nth-of-type(4) > div:nth-of-type(3)").click(); // fake click
        sleep(500);
        saveProductBtn.click();
    }

    @Step("Товар успешно добавлен.")
    public void isProductCreated(String productSummary) {
        $x(String.format("//table//td//span[contains(text(), '%s')]", productSummary)).should(visible);
        //$x(By.className("medium_txt")).waitUntil((Condition.text(productSummary)), 60000);
    }

    @Step("Видалити одну позицію.")
    public Goods deleteFirstPosition() {
        $$("global-products-price [role='presentation'] [role='row'] .dx-checkbox-icon").get(1).click();
        $("#gridDeleteSelected").click();
        return this;
    }

    /** Assert actions for price custom fields */
    @Step("Пользовательское поле присутствует в модальном окне добавления товара.")
    public void isCustomFieldPresentedAtAddingProductPopup(String fieldName) {
        isCustomFieldPresentedOnDropdown(fieldName);
        $("[data-bind] .dx-icon-close").click();
        isCustomFieldPresentedAtTable(fieldName);
    }

    @Step("Пользовательское поле присутствует в таблице.")
    public Goods isCustomFieldPresentedAtTable(String fieldName) {
        clickOnAddNewProductButton();
        $$("[data-bind] [aria-selected='false'] [role='presentation']:nth-of-type(3)")
                .findBy(text(fieldName)).shouldBe(visible);
        return this;
    }

    @Step("Пользовательское поле присутствует в выпадающем списке.")
    public void isCustomFieldPresentedOnDropdown(String fieldName) {
        $("global-products-price [aria-selected='false']:nth-of-type(2) .dx-dropdowneditor-icon").click();
        $(".dx-scrollview-content [role='option']:nth-of-type(8) .dx-list-item-content").click();
        $("global-products-price dx-data-grid .ng-star-inserted:nth-child(2) [aria-haspopup]").click();
        sleep(3000);
        $$x("//*[@class='dx-item-content dx-list-item-content']")
                .findBy(text(fieldName)).shouldBe(visible);
    }

    /** for testing demo limits */
    @Step("Загрузка прайс-листа.")
    public Goods uploadPrice(String priceList) {
        clickOnPriceAutoUploadingButton();
        selectPriceFormatExcel();
        selectImportTypeFromFile();
        uploadPriceListFile(priceList);
        clickNext();
        return this;
    }
}