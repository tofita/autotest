package com.autotest.zprice.test.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.autotest.zprice.test.helpers.UsefulMethods;
import io.qameta.allure.Step;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.autotest.zprice.test.helpers.Action.click;

public class Stores {

    final private SelenideElement
            resetFiltersIcon = $(".chip-close.fa-eraser"),
            confirmSalesTrackingButton = $("[data-bind] [role='toolbar']:nth-of-type(3) " +
                    ".dx-toolbar-button:nth-of-type(1) [role]"),
            saveStoreButton = $(".dx-popup-draggable.dx-popup-normal.dx-resizable > div:nth-of-type(3)  " +
                    ".dx-toolbar-after > div:nth-of-type(1) > .dx-item-content.dx-toolbar-item-content > " +
                    "div[role='button'] > .dx-button-content"),
            confirmRemoveBtn = $(".dx-popup-draggable.dx-popup-normal.dx-resizable " +
                    "> div:nth-of-type(3) > .dx-toolbar-items-container > .dx-toolbar-before > div:nth-of-type(1) " +
                    "> .dx-item-content.dx-toolbar-item-content > div[role='button']"),

    favoriteButton = $("#shopsSortBtn"),

    additionalInfoTab = $(".nav.nav-tabs > a:nth-of-type(5)"),
            companyNameInput = $("#note_details_companyNameField"),
            identCodeInput = $("#note_details_innField"),
            agentNameInput = $("#note_details_userNameField"),
            phoneInput = $("#note_details_phoneField"),
            emailInput = $("#note_details_emailField"),
            saveButton = $("app-brands-index-sites-table-notes .dx-button-content");
    private SelenideElement strLabel = $$("li > .custom-checkbox.custom-control > " +
            ".custom-control-label").first();
    private SelenideElement saveChangesButton = $("div.dx-toolbar.dx-widget." +
            "dx-visibility-change-handler.dx-collection.dx-popup-bottom > div > " +
            "div.dx-toolbar-after > div:nth-child(1) > div > div > div");

    private static ElementsCollection groupsList() {
        return $$("div#collapseGroups div > label");
    }

    private static ElementsCollection brandsList() {
        return $$("div#collapseBrands div > label");
    }

    private static ElementsCollection additionallyList() {
        return $$("div#collapseMore div > label");
    }

    private ElementsCollection storyIcon() {
        return $$("tr > td:nth-of-type(7)  i[title='История изменений']");
    }

    private ElementsCollection closedArrowIcon() {
        return $$("[aria-label='Развернуть']");
    }

    private ElementsCollection openedArrowIcon() {
        return $$(".dx-datagrid-group-opened");
    }

    private ElementsCollection wholeMarketStoresList() {
        return $$("#activatedMarketBlock .listBlock.menuScroll.selfScroll.vertical label");
    }

    private ElementsCollection mySitesList() {
        return $$("div#collapseShops div.scrollable-content > div > label");
    }


    private ElementsCollection storesTagList() {
        return $$("div#collapseShopsLabels span");
    }

    private ElementsCollection productTagList() {
        return $$("div#collapseProductLabels span");
    }

    private ElementsCollection mySitesLabelsList() {
        return $$(".listBlock.menuScroll.ng-star-inserted.selfScroll.vertical label");
    }

    /**
     * canAddReportToFavorites
     */
    @Step("Открыть окно со списокм отчётов и добавить один из них в избранное.")
    public Stores addReportToFavorite() {
        $("div#headerFixedToolbar").click();
        sleep(2000);
        $("[data-bind] .ng-star-inserted:nth-of-type(4) .fa-star").waitUntil(visible, 12000).click();
        return this;
    }

    @Step("Ввести название шаблона и указать кому доступен.")
    public Stores enterReportName(String reportName) {
        $("div.row > div > div > input")
                .waitUntil(visible, 12000).setValue(reportName);
        $("body [data-bind] [role='toolbar']:nth-of-type(3) .dx-toolbar-button:nth-of-type(1) [role]")
                .waitUntil(visible, 12000).click();
        return this;
    }

    /**
     * canFindStoreInTableList
     */
    @Step("В поле поиска ввести название сайта - {site}.")
    public Stores enterSiteName(String site) {
        $("div#contentBlock div > div > div > input").setValue(site);
        return this;
    }

    @Step("В результатах поиска есть искомый магазин - {site}.")
    public Stores isSiteDisplayedInTable(String site) {
        $(".dx-datagrid-search-text").shouldHave(text(site));
        return this;
    }

    /**
     * canFilterByGroupsAndBrands
     */
    @Step("Выбрать группу.")
    public Stores selectGroup() {
        groupsList().first().click();
        $("app-filters > .ng-star-inserted > div:nth-of-type(1)").waitUntil(visible, 5000);
        return this;
    }

    @Step("Выбрать брэнд.")
    public Stores selectBrand() {
        brandsList().first().click();
        $("app-filters > .ng-star-inserted > div:nth-of-type(2)").waitUntil(visible, 5000);
        sleep(3000);
        return this;
    }

    @Step("Отключить 'Только с РРЦ'")
    public Stores selectRRC() {
        additionallyList().first().click();
        return this;
    }

    private ElementsCollection goodsRows() {
        return $$("tr > td:nth-of-type(4)  .medium_txt");
    }

    @Step("В таблице присутствуют товары по заданным параметрам.")
    public void isFilteredByGroupsAndBrands() {

        String result = groupsList().first().getText() + " " + brandsList().first().getText();
        int rowsSize = goodsRows().size();
        for (int i = 0; i < rowsSize; i++) {
            goodsRows().get(i).waitUntil(text(result), 12000);
        }
    }

    /**
     * canAddNotesToStore
     */
    @Step
    public Stores addNotes(String companyName, String identCode, String agentName, String phone, String email) {
        expandStoresInfo(0);
        openAdditionalInfoTab();
        enterCompanyName(companyName);
        enterIdentCode(identCode);
        enterAgentName(agentName);
        enterPhoneNumber(phone);
        enterEmailAddress(email);
        clickOnSaveButton();
        return this;
    }

    @Step("Перейти во вкладку 'Заметки о магазине'.")
    public Stores openAdditionalInfoTab() {
        additionalInfoTab.click();
        return this;
    }

    @Step("Ввести название компании.")
    private void enterCompanyName(String companyName) {
        companyNameInput.setValue(companyName);
    }

    @Step("Ввести ИНН.")
    private void enterIdentCode(String identCode) {
        identCodeInput.setValue(identCode);
    }

    @Step("Ввести ФИО представителя.")
    public Stores enterAgentName(String agentName) {
        agentNameInput.setValue(agentName);
        return this;
    }

    @Step("Ввести Номер телефона.")
    private void enterPhoneNumber(String phoneNumber) {
        phoneInput.setValue(phoneNumber);
    }

    @Step("Ввести Email.")
    private void enterEmailAddress(String emailAddress) {
        emailInput.setValue(emailAddress);
    }

    @Step("Нажать кнопку 'Сохранить'.")
    public Stores clickOnSaveButton() {
        saveButton.click();
        return this;
    }

    @Step("Очистить поля.")
    public Stores clearNotes() {
        companyNameInput.clear();
        identCodeInput.clear();
        agentNameInput.clear();
        phoneInput.clear();
        emailInput.clear();
        return this;
    }

    @Step("Поля очищены.")
    public Stores isDataEmpty() {
        companyNameInput.shouldBe(empty);
        identCodeInput.shouldBe(empty);
        agentNameInput.shouldBe(empty);
        phoneInput.shouldBe(empty);
        emailInput.shouldBe(empty);
        return this;
    }

    @Step("Данные успешно сохранены.")
    public Stores isDataSaved(String companyName, String identCode, String agentName, String phone, String email) {
        companyNameInput.shouldHave(value(companyName));
        identCodeInput.shouldHave(value(identCode));
        agentNameInput.shouldHave(value(agentName));
        phoneInput.shouldHave(value(phone));
        emailInput.shouldHave(value(email));
        return this;
    }

    @Step("Редактировать данные.")
    public Stores editAdditionalInfo(String edited) {
        companyNameInput.append(edited);
        identCodeInput.append(edited);
        agentNameInput.append(edited);
        phoneInput.append(edited);
        emailInput.append(edited);
        clickOnSaveButton();
        return this;
    }

    /**
     * canAddStoreToFavoriteList
     */
    @Step("Нажать кнопку 'Добавить в избранное'.")
    private void clickOnAddToFavoritesButton() {
        favoriteButton.click();
    }

    @Step("Добавить в избранное магазин из списка.")
    public Stores addStoreToFavoriteList() {
        clickOnAddToFavoritesButton();
        String storeLabelText = $$("li > .custom-checkbox.custom-control > .custom-control-label")
                .first().getText();
        click(strLabel);
        $(".bold_txt.mediumText").waitUntil(text(storeLabelText), 12000);
        $("[data-bind] [class='col-6']:nth-of-type(1) .dx-scrollable-content").click();
        $$("[data-bind] [role='toolbar'] .dx-toolbar-button").get(1)
                .waitUntil(visible, 12000).click();
        return this;
    }

    @Step("Удалить магазин из избранного.")
    public Stores removeStoreFromFavoriteList() {
        clickOnAddToFavoritesButton();
        click(strLabel);
        $(".bold_txt.mediumText").waitUntil(not(visible), 12000);
        $("[data-bind] [class='col-6']:nth-of-type(1) .dx-scrollable-content").click();
        click(saveChangesButton);
        return this;
    }

    @Step("Магазин успешно добавлен в избранное.")
    public Stores isStoreAddedToFaveList() {
        $(".faa-parent.ng-star-inserted.orangeTxt.shopErrorBlock").shouldBe(visible);
        return this;
    }

    @Step("Магазин удалён из списка избранных.")
    public Stores isStoreRemovedFromFaveList() {
        $(".faa-parent.ng-star-inserted.orangeTxt.shopErrorBlock").shouldNotBe(visible);
        return this;
    }
    /**
     * canActivateSalesTrackingService
     */

    @Step("Нажать кнопку 'Начать отслеживать'.")
    private void clickOnStartTrackingButton() {
        $(".activateBtn.bg-orange.btn.mediumText.whiteTxt").waitUntil(visible, 10000).click();
    }

    @Step("Подтвердить активацию услуги 'Мониторинг акций'.")
    private void confirmActivationSalesTrackingService() {
        $("[data-bind] .text-danger").waitUntil(visible, 5000);
        click(confirmSalesTrackingButton);
    }

    /**
     * Filter actions
     */
    @Step("Лэйбл фильтра отображает указанный параметр.")
    private void filterLableShouldHaveCorrectParams(String param) {
        $(".chip.ng-star-inserted").waitUntil(visible, 20000);
        $("app-filters .ng-star-inserted:nth-of-type(1) .ng-star-inserted:nth-child(2)")
                .waitUntil(text(param), 20000);
    }

    @Step("Весь рынок. В фильтре 'Найденные сайты' кликнуть чекбокс с названием магазина.")
    public Stores filterByStoreFromWholeMarketList() {
        wholeMarketStoresList().get(1).waitUntil(visible, 5000).click();
        return this;
    }

    @Step("Мои магазины. В фильтре магазинов кликнуть чекбокс с названием магазина.")
    public Stores filterByStoreFromMyStoresList() {
        sleep(5000);
        mySitesList().first().click();
        return this;
    }

    @Step("В фильтре по меткам магазинов кликнуть чекбокс с меткой.")
    public Stores filterByStoresTags() {
        storesTagList().findBy(text("metka")).click();
        return this;
    }

    @Step("В фильтре по меткам товаров кликнуть чекбокс с меткой.")
    public Stores filterByProductTag() {
        productTagList().findBy(text("goods")).click();
        return this;
    }

    @Step("В результатах сортировки сверить выбранную метку магазина, с назавнием метки в таблице магазинов.")
    public Stores isFilteredByStoreTag() {
        $("[role='status']").waitWhile(appear, 60000);
        String strTag = storesTagList().first().getText();
        $("div#contentBlock tr:nth-child(1) > td:nth-child(3) > div > div")
                .waitUntil(text(strTag), 12000);
        filterLableShouldHaveCorrectParams(strTag);
        return this;
    }

    @Step("В результатах сортировки сверить выбранную метку товара, с назавнием метки лейбла фильтра.")
    public Stores isFilteredByProductTag() {
        String prdTag = productTagList().get(0).getText();
        $("div#contentBlock div.chip.ng-star-inserted").shouldHave(text(prdTag));
        filterLableShouldHaveCorrectParams(prdTag);
        return this;
    }

    @Step("В результатах сортировки сверить название выбранного магазина, с назавнием в таблице магазинов.")
    public Stores isFilteredBySite() {
        String labelText = mySitesList().first().getText();
        $(".dx-template-wrapper span:nth-child(1)").waitUntil(text(labelText), 45000);
        filterLableShouldHaveCorrectParams(labelText);
        return this;
    }

    @Step("Весь рынок. В результатах сортировки сверить название выбранного магазина, с назавнием в таблице магазинов.")
    public Stores isFilteredBySiteWM() {
        String labelText = wholeMarketStoresList().get(1).getText();
        $(".dx-template-wrapper span:nth-child(1)").waitUntil(text(labelText), 30000);
        filterLableShouldHaveCorrectParams(labelText);
        return this;
    }

    @Step("Очистить все фильтры.")
    public Stores resetAllFilters() {
        resetFiltersIcon.click();
        $(".chip.ng-star-inserted").shouldNotBe(visible);
        return this;
    }


    /**
     * canSeeStoryPopup
     */
    @Step("Развернуть дополнительную информацию.")
    public Stores expandStoresInfo(int i) {
        if(UsefulMethods.getLoaderStatus())
            closedArrowIcon().get(i).click();
        return this;
    }

    @Step("Свернуть дополнительную информацию.")
    public void collapseStoresInfo(int i) {
        openedArrowIcon().get(i).waitUntil(visible, 15000).click();
        openedArrowIcon().get(i).shouldNotBe(visible);
    }

    @Step("Кликнуть на иконку 'История'.")
    public Stores clickOnStoryIcon() {
        storyIcon().first().waitUntil(visible, 15000).click();
        return this;
    }

    @Step("Всплывающее окно истории открыто.")
    public Stores isStoryPopupDisplayed() {
        $("[data-bind] .dx-popup-content").waitUntil(visible, 20000);
        return this;
    }

    /**
     * canAddNewStoreToList
     */

    @Step("Добавить магазин {newStore} в список 'Мои магазины'.")
    public Stores addNewStoreToList(String newStore) {
        $("button#shopsAddBtn").waitUntil(visible, 3000).click();
        sleep(5000);
        $("dx-autocomplete > div > div > input").waitUntil(visible, 3000).setValue(newStore);
        sleep(3000);
        saveStoreButton.waitUntil(visible, 3000).click();
        return this;
    }

    @Step("Найти магазин в списке 'Мои магазины'.")
    public void findStore(String newStore) {
        $("#collapseShops [type='text']").setValue(newStore);
    }

    @Step("Удалить добавленный магазин из списка.")
    public Stores removeNewStoreFromList(String newStore) {
        findStore(newStore);
        mySitesLabelsList().first().hover();
        $("div#collapseShops  .listBlock.menuScroll.ng-star-inserted" +
                ".selfScroll.vertical div[title='Удалить'] > .fa.fa-sm.fa-times.faa-pulse")
                .waitUntil(visible, 10000).click();
        confirmRemoveBtn.waitUntil(visible, 3000).click();
        return this;
    }

    @Step("Новый магазин {newStore} добавлен в список.")
    public Stores isNewStoreAddedToList(String newStore) {
        mySitesLabelsList().first().shouldHave(text(newStore));
        return this;
    }

    // TODO: 16.01.2020 Переделать проверку.
    @Step("Новый магазин {newStore} удалён из списка.")
    public Stores isNewStoreRemovedFromList(String newStore) {
        mySitesLabelsList().first().shouldNotHave(text(newStore));
        return this;
    }

    @Step("Данные корректно отфильтрованы.")
    public Stores isProductsFilteredByTag() {
        $(".chip.ng-star-inserted").waitUntil(visible,5000);
        String prdTag = productTagList().get(0).getText();
        $$("tr > td:nth-of-type(2)  .labels-test").first().shouldBe(text(prdTag), visible);
        filterLableShouldHaveCorrectParams(prdTag);
        return this;
    }

    @Step("Добавить магазин")
    public Stores addNewStore() {
        $x("//button[@id='shopsAddBtn']").click();
        $x("//div[@class='dx-dropdowneditor-input-wrapper']//input[@class='dx-texteditor-input']").click();
        $x("//div[@class='dx-overlay-content dx-popup-normal dx-popup-draggable dx-resizable']//div[@class='dx-button dx-button-default dx-button-mode-contained dx-widget dx-button-has-text']").click();
        return this;
    }

    @Step("Отловить ошибку")
    public void addNewStoreCheckAlert() {
        $x("//div[@class='_toast-body advanced-toast-body font-weight-bold']").shouldHave(text("Имя конкурента не может быть пустым"));
    }

    @Step("Загружает ли данные по статистике уникальных товаров")
    public void checkUniqueProducts() {
        $("#headerFixedToolbar .ng-star-inserted:nth-child(4) [role]").click();
        $(".dx-popup-draggable.dx-popup-normal.dx-resizable div[role='alert']").shouldHave(text("Количество уникальных товаров за выбранный период:"));
    }
}