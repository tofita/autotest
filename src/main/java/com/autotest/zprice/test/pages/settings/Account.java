package com.autotest.zprice.test.pages.settings;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.value;
import  static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.autotest.zprice.test.helpers.Action.click;

public class Account {

    final public SelenideElement
            accountTab = $("[role] [queryparamshandling='merge']:nth-of-type(2)"),
            companyNameInput = $("[formcontrolname='ClientName']"),
            representativeInput = $("[formcontrolname='Representative']"),
            addressInput = $("[formcontrolname='Address']"),
            bankNameInput = $("[formcontrolname='Bank']"),
            identNumberInput = $("[formcontrolname='INN']"),
            edrpouInput = $("[formcontrolname='EDRPOU']"),
            mfoInput = $("[formcontrolname='MFO']"),
            bankAccountNumberInput = $("[formcontrolname='BankAccountNumber']"),
            saveButton = $("div > div:nth-of-type(1) > .btn.btn-primary"),

            demoTourCheckbox = $("[for='showDemoTourCheckbox']"),
            confirmDemoTourBtn = $("._toast-toolbar_actionButton"),
            hideDemoTourBtn = $("[data-bind] .dx-toolbar-before .dx-button-text"),

            contactPersonInput = $x("//input[contains(@placeholder,'Введите контактное лицо')]"),
            emailInput = $x("//input[contains(@placeholder, 'Введите новый email')]"),
            phoneNumberInput = $x("//input[contains(@placeholder, 'Введите номер телефона')]"),
            savePersonalDataBtn = $(".container .ng-star-inserted:nth-child(4) button");

    /** canEditPersonalData */
    @Step("Заполнить поля персональных данных.")
    public Account editData(String contacts, String email, String tel) {
        switchToAccountTab();
        enterContactPersonName(contacts);
        enterEmailAddress(email);
        enterPhoneNumber(tel);
        clickSavePersonalDataButton();
        return this;
    }

    @Step("Указать контактное лицо.")
    private void enterContactPersonName(String contacts) {
        contactPersonInput.setValue(contacts);
    }

    @Step("Ввести email.")
    private void enterEmailAddress(String email) {
        emailInput.setValue(email);
    }

    @Step("Ввести номер телефона.")
    private void enterPhoneNumber(String phone) {
        phoneNumberInput.setValue(phone);
    }

    @Step("Нажать кнопку 'Сохранить'.")
    private void clickSavePersonalDataButton() {
        savePersonalDataBtn.click();
    }

    @Step("Сверяем измененные персональные данные.")
    public void isPersonalDataChanged(String contacts, String email, String tel) {
        contactPersonInput.shouldHave(value(contacts)).waitUntil(visible, 5500);
        emailInput.shouldHave(value(email)).waitUntil(visible, 5500);
        phoneNumberInput.shouldHave(value(tel)).waitUntil(visible, 5500);
    }

    /** canEnableOrDisableDemoTour */
    @Step
    public void manageDemoTour() {
        switchToAccountTab();
        clickOnDemoTourCheckBox();
        confirmEnabling();
    }

    @Step("Кликнуть чекбокс 'Показывать демо-тур при открытии программы'.")
    private Account clickOnDemoTourCheckBox() {
        demoTourCheckbox.click();
        return this;
    }

    @Step("Подтвердить отображение демо-тура и перезагрузить приложение.")
    private Account confirmEnabling() {
        confirmDemoTourBtn.waitUntil(visible, 3000);
        confirmDemoTourBtn.click();
        return this;
    }

    @Step("Должно появиться окно с информацией о демо-туре")
    public boolean isTourInfoEnabled() {
        $("[data-bind] > [tabindex]").shouldBe(visible);
        return true;
    }

    @Step("Окна с информацией о демо-туре быть не должно.")
    public boolean isTourInfoDisabled() {
        $("[data-bind] > [tabindex]").shouldNotBe(visible);
        return false;
    }

    @Step("Нажать кнопку 'Не показывать снова'.")
    public void clickOnDoNotShowAgainButton() {
        sleep(6000);
        click(hideDemoTourBtn);
        sleep(2000);
    }

    /** canEditRequisites */
    @Step("Заполнить поля реквизитов.")
    public Account editRequisites(String companyName, String representative, String address, String bankName,
                               String identNumber, int edrpou, int mfo, int bankNumber) {
        switchToAccountTab();
        enterCompanyName(companyName);
        specifyRepresentative(representative);
        enterAddress(address);
        enterBankName(bankName);
        enterIdentNumber(identNumber);
        enterEdrpou(edrpou);
        enterMfo(mfo);
        enterBankAccountNumber(bankNumber);
        clickOnSaveButton();
        return this;
    }

    @Step("Переключиться на вкладку 'Аккаунт'")
    public Account switchToAccountTab() {
        accountTab.click();
        return this;
    }

    @Step("Ввести название компании.")
    private void enterCompanyName(String companyName) {
        companyNameInput.setValue(companyName);
    }

    @Step("Указать представителя.")
    private void specifyRepresentative(String represent) {
        representativeInput.setValue(represent);
    }

    @Step("Ввести адрес")
    private void enterAddress(String address) {
        addressInput.setValue(address);
    }

    @Step("Указать название банка.")
    private void enterBankName(String bankName) {
        bankNameInput.setValue(bankName);
    }

    @Step("Ввести ИНН.")
    private void enterIdentNumber(String innNumber) {
        identNumberInput.setValue(innNumber);
    }

    @Step("Ввести ЕДРПОУ.")
    private void enterEdrpou(int edrpou) {
        edrpouInput.setValue(String.valueOf(edrpou));
    }

    @Step("Указать МФО.")
    private void enterMfo(int mfo) {
        mfoInput.setValue(String.valueOf(mfo));
    }

    @Step("Ввести номер расчётного счёта.")
    private void enterBankAccountNumber(int bankNumber) {
        bankAccountNumberInput.setValue(String.valueOf(bankNumber));
    }

    @Step("Нажать кнопку 'Сохранить'.")
    private void clickOnSaveButton() {
        saveButton.click();
    }

    @Step("Сверяем измененные данные реквизитов.")
    public Account isRequisitesDataChanged(String companyName, String representative, String address, String bankName,
                                        String identNumber, int edrpou, int mfo, int bankNumber) {
        companyNameInput.shouldHave(value(companyName));
        representativeInput.shouldHave(value(representative));
        addressInput.shouldHave(value(address));
        bankNameInput.shouldHave(value(bankName));
        identNumberInput.shouldHave(value(identNumber));
        edrpouInput.shouldHave(value(String.valueOf(edrpou)));
        mfoInput.shouldHave(value(String.valueOf(mfo)));
        bankAccountNumberInput.shouldHave(value(String.valueOf(bankNumber)));
        return this;
    }
}