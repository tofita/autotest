package com.autotest.zprice.test.pages;

import com.autotest.zprice.test.pages.settings.Account;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.autotest.zprice.test.helpers.Action.click;

public class Index {

    private static final SelenideElement
            loginButton = $x("//a[@id='main-sign-in-btn']");
    private static final SelenideElement//loginButton = $(".s_welcome__text [data-toggle='modal']:nth-child(4)"),
            loginInput = $("#authLoginInput");
    private static final SelenideElement passwordInput = $("#authPassInput");
    private static final SelenideElement submitButton = $("#authBtn");

    @Step("Нажать кнопку 'Вход в систему'.")
    public static void clickOnLoginButton() {
        loginButton.click();
    }

    @Step("Ввести логин пользователя.")
    public static void enterLogin(String username) {
        loginInput.setValue(username);
    }

    @Step("Ввести пароль пользователя.")
    public static void enterPassword(String pwd) {
        passwordInput.setValue(pwd);
    }

    @Step("Нажать кнопку 'Войти'.")
    public static void clickOnSubmitButton() {
        submitButton.click();
    }

    @Step("Увійти до системи")
    public static void signIn(String login, String password) {
        clickOnLoginButton();
        enterLogin(login);
        enterPassword(password);
        clickOnSubmitButton();
    }
}