package com.autotest.zprice.test.pages.settings;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class Company {
    final private SelenideElement
            companyLink = $x("//div[@class='structureBlock']//a[4]"),
            btnAddCompapny = $x("//button[@class='btn btn-light btn-small blueTxt']"),
            inputCompanyName = $x("//dx-text-box[@class='dx-textbox dx-texteditor dx-editor-outlined dx-texteditor-empty dx-widget']//input[@class='dx-texteditor-input']"),
            selectCountry = $x("//div[@class='dx-dropdowneditor-icon']");
    @BeforeTest
    public Company local(){
        
        return this;
    }


    @Step("Открыть вкладку \"Компании\"")
    public Company clickCompanyLink(){
        companyLink.click();
        return this;
    }

    @Step("Нажать на кнопку добавить")
    public Company clickAddBtn(){
        btnAddCompapny.click();
        return this;
    }

    @Step("Ввести название компании")
    public Company inputCompanyName(String companyName){
        inputCompanyName.setValue(companyName);
        return this;
    }

    @Step("Выбрать страну")
    public Company selectCountry(){
        selectCountry.click();
        ElementsCollection items =  $$x("//div[@id='dx-604b51dd-b0bb-f806-f384-71f05a1d8e24']//div[@class='dx-scrollview-content']");
        items.get(0).click();
        return this;
    }





}


